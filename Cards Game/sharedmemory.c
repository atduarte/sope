#include "types.h"

// Global Variables

int n=0;
int handLenght=0;
char ownCards[52][4];
GameMem* shm;
int player_id;
char receivedCards[52][4];
int cardsToGive;
pthread_t tid1,tid2;

// Functions

int unlinkmemory();

int cardValue(char* card);

void sortCards(int option);

void displayhand();

void printCards(char* line,int option);

void play();

void displayprevioustable();

void displaytable();

void* waitPlayer(void *arg);

void* displaymenuplay(void *arg);

void sigint_handler(int signo)
{
   //terminar os outros e fazer unlink + fifos
   printf("SIGNAL RECEIVED\n");
   printf("THE GAME WILL END SOON\n");
   int i=0;
   for(i=0;i<shm->nPlayers;i++)
      if(player_id!=shm->players[i].id)
         kill(shm->players[i].process_id, SIGUSR1);
   unlinkmemory();
   exit(0);
}


void sigusr1_handler(int signo)
{
   printf("THE END \n");
   exit(0);
}


int main(int argc, char *argv[])
{

   //
   // Verificações
   // 

   if(argc!=4)
   {
      printf("Error: Input must be: name table max_players\n");
      exit(1);
   }

   if(atoi(argv[3])>=52 || atoi(argv[3])<=1)
   {
      printf("ARGV[3] must be between 2 and 52\n");
      exit(1);
   }

   srand (time(NULL));

   //
   // Sinais
   // 

   if(signal(SIGINT,sigint_handler)<0)
   {
      fprintf(stderr,"Unable to install SIGINT handler");
   }

   if(signal(SIGUSR1,sigusr1_handler) <0)
   {
      fprintf(stderr,"Unable to install SIGUSR1 handler");
   }
   setbuf(stdout, NULL);

   //
   // Criação do FIFO do jogador
   //



   

   //
   // Criação de Memória Partilhada
   //

   char* gameMemName = malloc(strlen(argv[2]) + 9);
   strcpy(gameMemName, "gameMem_");
   strcat(gameMemName, argv[2]);

   int shmfd;
   int newMem = 0;
  
   // Verifica se já está criada
   shmfd = shm_open(gameMemName, O_RDWR,0660);

   // Se não estiver criada
   if(shmfd<0)
   {
      newMem = 1;

      shmfd = shm_open(gameMemName, O_CREAT|O_RDWR,0660);

      //specify the size of the shared memory region
      if(ftruncate(shmfd,sizeof(GameMem)) < 0)
      {
         perror("Failure in ftruncate()");
         exit(1);
      }
   }

   //attach this region to virtual memory
   shm = mmap(0,sizeof(GameMem),PROT_READ|PROT_WRITE,MAP_SHARED,shmfd,0);
   if(shm == MAP_FAILED)
   {
      perror("Failure in mmap()");
      exit(1);
   }

   //
   // Introdução de dados na memória partilhada
   //

   if(newMem) {

      strcpy(shm->mesa,gameMemName);
      shm->nPlayers = 1;
      shm->nSits = atoi(argv[3]);
      shm->dealer = 0;


      // Criação do Semáforo
      sem_init(&(shm->full), SHARED, 0);
      sem_init(&(shm->cardsGiven), SHARED, 0);
      sem_init(&(shm->unique), SHARED, 1);
      sem_init(&(shm->newturn), SHARED, 0);
      sem_init(&(shm->fifosReady), SHARED, 0);

      int i;
      for(i = 0; i < shm->nSits; i++) {
         sem_init(&(shm->players[i].play), SHARED, 0);
         sem_init(&(shm->players[i].ready), SHARED, 0);
      }

      // Cartas
      char rank[13];

      rank[0]='A';
      rank[1]='2';
      rank[2]='3';
      rank[3]='4';
      rank[4]='5';
      rank[5]='6';
      rank[6]='7';
      rank[7]='8';
      rank[8]='9';
      rank[9]='0';
      rank[10]='J';
      rank[11]='Q';
      rank[12]='K';
   
      int suit[4];
      suit[0] = (int)('c');
      suit[1] = (int)('d');
      suit[2] = (int)('h');
      suit[3] = (int)('s');

      int j;
      for(i = 0; i < 13; i++) {
         for(j = 0; j < 4; j++) {
            if(rank[i] != '0') {
            shm->cards[i*4+j][0] = rank[i];
            shm->cards[i*4+j][1] = suit[j];
            shm->cards[i*4+j][2] = '\0';
            } else {
               shm->cards[i*4+j][0] = '1';
               shm->cards[i*4+j][1] = rank[i];
               shm->cards[i*4+j][2] = suit[j];
               shm->cards[i*4+j][3] = '\0';
            }
            
         }
      }



      //shuffle      
      for(i=0;i<52;i++)
      {
         int v1 = (rand() % 52 );
         char card[4];
         strcpy(card,shm->cards[i]);
         strcpy(shm->cards[i],shm->cards[v1]);
         strcpy(shm->cards[v1],card);
      }
   } else {

      if(shm->nSits != atoi(argv[3]) || shm->nPlayers >= shm->nSits ) 
      {
         printf("Invalid number of players.\n");
         exit(1);         
      } 
      else 
      {
         if(strlen(argv[1])>6)
         {
            printf("Max Lenght ARGV[1] = 6\n");
            exit(1);
         }

         /*
         int i=0;
         int encontrou=0;
         for(i=0;i<shm->nPlayers;i++)
         {
            if(strcmp(shm->players[i].name,argv[1])==0)
            {
               encontrou=1;
               break;            
            }
         }

         if(encontrou==1)
         {
            printf("Another player registered with same name\n");
            exit(1);
         }*/

      }

      shm->nPlayers += 1;
   }

   // Introduzir Jogador
   sem_wait(&(shm->unique));


   char* fifoName = malloc(strlen(argv[2]) + 2);


   strcpy(fifoName, argv[2]);
   printf("%s\n",fifoName);

   strcat(fifoName, "_");
   printf("%s\n",fifoName);

   sprintf(fifoName, "%s%d", fifoName,shm->nPlayers-1);


   printf("%s\n",fifoName);


   mkfifo(fifoName, 0666);

   player_id = shm->nPlayers-1;
   shm->players[shm->nPlayers-1].id = shm->nPlayers-1;
   strcpy(shm->players[shm->nPlayers-1].name, argv[1]);
   shm->players[shm->nPlayers-1].process_id=getpid();
   strcpy(shm->players[shm->nPlayers-1].fifoName, fifoName);

   // Libertar o semaforo para a memória
   sem_post(&(shm->unique));

   if(shm->nPlayers == shm->nSits) {
      printf("%s complete: game may start\n", argv[2]);
      printf("Dealer is %s\n", shm->players[shm->dealer].name);
   } else {
      printf("...waiting for other players...\n");
      sem_wait(&(shm->full));
      printf("Connected.\n");
   }
   
   sem_post(&(shm->full));

   int fifo[shm->nPlayers];
   int fifoD;

   // Abrir FIFOS
   if(player_id==shm->dealer) 
   {
      fifo[0]=open(shm->players[0].fifoName,O_RDWR);

      int j;
      for(j = 1; j < shm->nPlayers; j++) {        
         sem_wait(&(shm->players[j].ready));
         fifo[j]=open(shm->players[j].fifoName,O_WRONLY);
      }

      sem_post(&(shm->fifosReady));

   } else {      
      sem_post(&(shm->players[player_id].ready));
      fifo[player_id] = open(shm->players[player_id].fifoName,O_RDONLY);
   }

   time_t t;

   //
   // DEALER: Atribui Cartas
   //

   struct tm * timeinfo;

   char * timeString;
   cardsToGive = 52.0/(shm->nPlayers);

   if(player_id==shm->dealer)
   {
      FILE *f;
    
      strcpy(shm->filename,argv[2]);
      strcat(shm->filename,".log");
      //printf("Nome do ficheiro: %s\n",shm->filename);

      f = fopen(shm->filename, "w");
      char *cabe="when                 | who            | what           | result";  
      fprintf(f, "%s\n", cabe);
      char line[500];
      time (&t);
   
      timeinfo = localtime(&t);
      (*timeinfo).tm_isdst = 0;
      timeString = malloc(150);
      strftime(timeString,150, "%G-%m-%d %H:%M:%S ", timeinfo);

      strcpy(line,timeString);
      strcat(line," | ");
      strcat(line,"Dealer-");
      strcat(line,shm->players[0].name);
      int k=0;
      for(k=0;k<7-strlen(shm->players[0].name);k++)
         strcat(line," ");
      strcat(line," | ");
      strcat(line,"deal          ");
      strcat(line," | ");
      strcat(line,"-");

      fprintf(f, "%s\n", line);     
    

      int i, j;
      for(j= 0; j < shm->nPlayers; j++) {
         //for(i=cardsToGive*j;i<(cardsToGive*(j+1));i++) { 
         for(i=0;i<cardsToGive;i++) { 
            int a = write(fifo[j],shm->cards[j*cardsToGive+i],sizeof(shm->cards[j*cardsToGive+i]));
        
            strcpy(receivedCards[i],shm->cards[j*cardsToGive+i]);
         }

         sortCards(1);

         char line2[600];
 
         time (&t);
         timeinfo = localtime(&t);
         (*timeinfo).tm_isdst = 0;
         timeString = malloc(150);
         strftime(timeString,150, "%G-%m-%d %H:%M:%S ", timeinfo);  
         strcpy(line2,timeString);
 
         strcat(line2," | Player");
         char numer2[10];
         snprintf(numer2, 10,"%d",j);
         strcat(line2,numer2);
         strcat(line2,"-");
 
         strcat(line2,shm->players[j].name);
  
         for(k=0;k<6-strlen(shm->players[j].name);k++)
         {
            strcat(line2," ");
         }
     
         strcat(line2," | receive_cards  | ");
         printCards(line2,1);
    
         fprintf(f, "%s\n", line2);
      }

      fclose(f);
   }

   //
   // Recebe Cartas
   //

   handLenght=cardsToGive;
   int count=0;
   do {      
      read(fifo[player_id],ownCards[count],4); 
      count++;     
   } while(count<cardsToGive);


   int firstplayer=1;

   //
   // Fechar FIFOS
   //

   int j;
   if(player_id==shm->dealer)
      for(j= 0; j < shm->nPlayers; j++)
         close(fifo[j]);
   else
      close(fifo[player_id]);

   //
   // Inicializar Primeira Ronda
   //

   sortCards(0);

   shm->turn=0;
   shm->roundNumber=1;
   shm->nextPlayer=1;

   if(player_id==shm->dealer)
      sem_post(&(shm->players[shm->nextPlayer].play));

   //
   // Play
   //

   while(1)
   {
      //semaforo
      if(shm->nextPlayer != player_id)
         pthread_create(&tid1,NULL,displaymenuplay,NULL);
      pthread_create(&tid2,NULL,waitPlayer,NULL);
      pthread_join(tid2);

      if(shm->ended == 1)
      {
         kill(shm->players[player_id].process_id,SIGINT);
      }

      int i=0;
      if(shm->dealer==player_id)
         for(i=0;i<shm->nPlayers;i++)
            strcpy(shm->previoustable[i],shm->table[i]);
   } 

   return 0;
}

int unlinkmemory()
{  
   char gameMemName[100];
   strcpy(gameMemName,shm->mesa);
   
   int i=0;
      for(i=0;i<shm->nPlayers;i++)
         remove(shm->players[i].fifoName);

   int shmfd = shm_open(gameMemName, O_CREAT|O_RDWR,0660);

   //specify the size of the shared memory region
    if(ftruncate(shmfd,sizeof(GameMem)) < 0)
    {
       perror("Failure in ftruncate()");
       return 0;
    }

   GameMem* shm = mmap(0,sizeof(GameMem),PROT_READ|PROT_WRITE,MAP_SHARED,shmfd,0);

   if(munmap(shm, sizeof(GameMem)) < 0)
      return -1;
   if(shm_unlink(gameMemName) < 0)
      return -1;

   printf("Done\n");
   return 0;
}

int cardValue(char* card) 
{

   int naipe = 0;
   int numero = 0;

   if(strlen(card) <= 0)
      return 54;

   switch(card[strlen(card)-1]) {
      case 'c': 
         naipe = 0;
         break;
      case 'd':
         naipe = 1;
         break;
      case 'h':
         naipe = 2;
         break;
      case 's':
         naipe = 3;
         break;
   }

   switch(card[0]) {
      case 'A':
         numero = 1;
         break;
      case 'J':
         numero = 11;
         break;
      case 'Q':
         numero = 12;
         break;
      case 'K':
         numero = 13;
         break;
      case '1':
         numero = 10;
         break;
      default:
         numero = (int)card[0]-48;
         break;
   }

   return (naipe * 13) + numero;
}

void sortCards(int option) 
{

   char cards[52][4];
 
   if(option==0)
   {
      int i, j;
      for(i = 0; i < handLenght; i++) {
         int menor = 54, k = 54;
   
         for(j = 0; j < handLenght; j++) {
            int tmp = cardValue(ownCards[j]);
            if(tmp < menor) {
               menor = tmp;
               k = j;
            }
         }
         if(menor < 54) {
            strcpy(cards[i], ownCards[k]);
            ownCards[k][0] = '\0';
         }
      }
      for(i = 0; i < handLenght; i++)
         strcpy(ownCards[i], cards[i]);
   }
   else if(option==1)
   {
      int i, j;
      for(i = 0; i < cardsToGive; i++) {
         int menor = 54, k = 54;
   
         for(j = 0; j < cardsToGive; j++) {
            int tmp = cardValue(receivedCards[j]);
            if(tmp < menor) {
               menor = tmp;
               k = j;
            }
         }
         if(menor < 54) {
            strcpy(cards[i], receivedCards[k]);
            receivedCards[k][0] = '\0';
         }
      }
      for(i = 0; i < cardsToGive; i++)
         strcpy(receivedCards[i], cards[i]);
   }  
}

void displayhand()
{
   int i=0;
   for(i=0;i<handLenght-1;i++)
   {
      printf("%s",ownCards[i]);
      if(ownCards[i][strlen(ownCards[i])-1]!=ownCards[i+1][strlen(ownCards[i+1])-1])
         printf("/");
      else
         printf("-");
   }
   printf("%s\n",ownCards[handLenght-1]);
   return;
}


void printCards(char* line,int option)
{
   if(option==0)
   {
      int i=0;
      for(i=0;i<handLenght-1;i++)
      {
         strcat(line,ownCards[i]);
         if(ownCards[i][strlen(ownCards[i])-1]!=ownCards[i+1][strlen(ownCards[i+1])-1])
            strcat(line,"/");
         else
            strcat(line,"-");
      }
      strcat(line,ownCards[handLenght-1]);
   }
   else
   {

      int i=0;
      for(i=0;i<cardsToGive-1;i++)
      {
         strcat(line,receivedCards[i]);
         if(receivedCards[i][strlen(receivedCards[i])-1]!=receivedCards[i+1][strlen(receivedCards[i+1])-1])
            strcat(line,"/");
         else
            strcat(line,"-");
      }
      strcat(line,receivedCards[cardsToGive-1]);
   }
   return;
}

void play()
{

   FILE *f;
   f = fopen(shm->filename, "a");

 
   displayhand();
   int indice;
   
   while(1)
   {
      printf("Introduzir o id da Carta: \n");

      scanf("%d",&indice); 
      if(indice<handLenght)
      {
         break;
      }

   }

   char line[500];

   time_t t;

   //atribuir cartas
   struct tm * timeinfo;

   char * timeString;

   time (&t);
  
   timeinfo = localtime(&t);
   (*timeinfo).tm_isdst = 0;
   timeString = malloc(150);
   strftime(timeString,150, "%G-%m-%d %H:%M:%S ", timeinfo);

   strcpy(line,timeString);
   strcat(line," | Player");

   char numer[10];
   snprintf(numer, 10,"%d",player_id);  
   strcat(line,numer);
   strcat(line,"-");
   strcat(line,shm->players[player_id].name);

   int k=0;
   for(k=0;k<6-strlen(shm->players[player_id].name);k++)
      strcat(line," ");
   strcat(line," | play           | ");
   
   char carta[10];
   strcpy(carta,ownCards[indice]);
   strcat(line,carta);
 
   fprintf(f, "%s\n", line);
   
   strcpy(shm->table[shm->turn],ownCards[indice]);

   int j;
   for(j = indice; j + 1 < handLenght; j++) 
   {        
      char tmpCard[4];
      strcpy(tmpCard,ownCards[j]);
      strcpy(ownCards[j],ownCards[j+1]);
      strcpy(ownCards[j+1],tmpCard);
   }
   handLenght--;

   char line2[400];

   time (&t);
   timeinfo = localtime(&t);
   (*timeinfo).tm_isdst = 0;
   timeString = malloc(150);
   strftime(timeString,150, "%G-%m-%d %H:%M:%S ", timeinfo);   
   strcpy(line2,timeString);

   strcat(line2," | Player");
   char numer2[10];
   snprintf(numer2, 10,"%d",player_id);
   strcat(line2,numer2);
   strcat(line2,"-");

   strcat(line2,shm->players[player_id].name);

   for(k=0;k<6-strlen(shm->players[player_id].name);k++)
      strcat(line2," ");
 
   strcat(line2," | hand           | ");
   printCards(line2,0);
   fprintf(f, "%s\n", line2);
   fclose(f);

   shm->turn++;
   return;
}

void displayprevioustable()
{
   int i=0;
   if(shm->roundNumber>1)
   {
      for(i=0;i<shm->nPlayers-1;i++)
         printf("%s - ",shm->previoustable[i]);

      printf("%s\n",shm->previoustable[shm->nPlayers-1]);
   }
}

void displaytable()
{
   int i;
   for(i=0;i<shm->turn-1;i++)
      printf(" %s ",shm->table[i]);

   printf(" %s \n",shm->table[shm->turn-1]);
}

void* waitPlayer(void *arg)
{
   sem_wait(&(shm->players[player_id].play));
   n=1;
   pthread_cancel(tid1);

   if(!shm->ended) {
      pthread_create(&tid1,NULL,displaymenuplay,NULL);
      pthread_join(tid1);

      if(shm->turn==shm->nPlayers)
         shm->turn=0;

      if(player_id == 1)         
         shm->roundNumber++;
      
      shm->nextPlayer++;
      if(shm->nextPlayer==shm->nPlayers) 
         shm->nextPlayer=0;

      if(handLenght == 0 && player_id == shm->dealer)
         shm->ended = 1;
   }
   sem_post(&(shm->players[shm->nextPlayer].play));
   return NULL;
}

void* displaymenuplay(void *arg)
{
   int op=0;

   time_t start,end; 
   time (&start);

   while(1)
   {
         if(n==1)
         {
            printf("\nYOUR TURN\n");
            printf("\n---------\n");
            printf("1 - Play\n"); 
         }
         printf("2 - Show the cards on table\n");
         printf("3 - Show the players's own hand\n");
         printf("4 - Show previous round\n");
         if(n==1)
         {
            printf("5 - Show the times of the game\n");
         }
            
         scanf("%d",&op);
     
         if(n==1 && op==1)
         {
            play();
            n=0;
            return;
         }
         else if(op==2)
         {
            displaytable();
         }
         else if(op==3)
         {
            displayhand();
         }
         else if(op==4)
         {
            displayprevioustable();
         }
         else if(op==5 && n==1)
         {
            time (&end);
            double dif = difftime (end,start); 
            printf ("Elasped time is %0.2f seconds.", dif ); 
         }
   }

   return NULL;
}