#include "stdlib.h"
#include "stdio.h"
#include "unistd.h"
#include "errno.h"
#include "string.h"
#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>

#include <sys/mman.h>


 

#include <fcntl.h>
#include <time.h>       /* time */
#include <semaphore.h>
#include <math.h>

#define SHARED 1

typedef struct
{
   char rank;
   char suit;
} Card;

typedef struct
{
   int id;
   char name [256];
   char fifoName [269];
   sem_t play, ready;
   pid_t process_id;
} Player;

typedef struct
{
   char mesa[100];
   char filename[100];
   char table[52][4];
   char previoustable[52][4];
   int nSits;
   int nPlayers;
   int dealer;
   Player players[100];
   int roundNumber;
   int turn;
   char cards[52][4];
   int nextPlayer;
   int ended;
   sem_t full, unique, cardsGiven, fifosReady, newturn;
} GameMem;